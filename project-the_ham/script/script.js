// OUR SERVICES active tabs

const $tab = $('.services-navbar .services-navbar-link'); //добавляем для активной навигации верхний бордер
$tab.on('click', function () {
    $tab.removeClass('active');
    $(this).addClass('active');

    $(".services-about")                //
        .removeClass("clicked")
        .eq($(this).index())
        .addClass("clicked");
});



// OUR AMAZING WORK

// active tabs

const $tabs = $('.work-wrapper .work-links');
$tabs.on('click', function () {


    const $workImg = $('.work-img');

    $workImg.hide();
    $tabs.removeClass('actives');
    $(this).addClass('actives');

    const $image = document.getElementsByClassName($(this).attr('data-type'));


    for (let i = 0; i < 24; i++) {
        $($image[i]).show();
    }

});


// click button = more img

const $button = $('#button');
$button.on('click', function () {

   $('.loader').toggleClass('work-hidden');


    setTimeout(function () {
       $('.loader').toggleClass('work-hidden');
       document.querySelector('.work-images.work-hidden').style.display = 'flex';
   }, 2000);
    $button.css({display: 'none'});

});



//hover BREAKING-NEWS on links



$('.slider-for').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    asNavFor: '.slider'
});
$('.slider').slick({
    infinite: true,
    slidesToShow: 4,
    centerPadding: '1px',
    arrows: true,
    slidesToScroll: 1,
    asNavFor: '.slider-for',
    centerMode: true,
    focusOnSelect: true,
    initialSlide: 0,
});

$('.people-hasan-mini').on('click', function () {
    $('.people-hasan-mini').removeClass('border-img');
    $(this).addClass('border-img');
    $('.people-hasan').hide();
    const $people = document.getElementsByClassName($(this).attr("data-name"));
    for (let i = 0; i < 4; i++) {
        $($people[i]).show();
    }
});

$('.slick-arrow').on('click', function () {
    $('.people-hasan-mini').removeClass('border-img');
    $('.slick-center > div > img').addClass('border-img');
    $('.people-hasan').hide();

    const $people = document.getElementsByClassName($('.slick-center > div > img').attr("data-name"));
    for (let i = 0; i < 4; i++) {
        $($people[i]).show();
    }
});