function math() {
    let firstNumber = Number(prompt('Enter your first number'));
    while (firstNumber == '' || isNaN(firstNumber) || firstNumber === null) {
        firstNumber = Number(prompt('Enter your first number'));
    }
    let secondNumber = Number(prompt('Enter your second number'));
    while (secondNumber == '' || isNaN(secondNumber) || secondNumber === null) {
        secondNumber = Number(prompt('Enter your second number'));
    }
    let userOperation = prompt('Enter your operation');
    while (userOperation !== '+' && userOperation !== '-' && userOperation !== '*' && userOperation !== '/') {
        userOperation = prompt('Enter your operation');
    }
    switch (userOperation) {
        case '+':
            return firstNumber + secondNumber;
        case '-':
            return firstNumber - secondNumber;
        case '*':
            return firstNumber * secondNumber;
        case '/':
            return firstNumber / secondNumber;
    }
}
console.log(math());