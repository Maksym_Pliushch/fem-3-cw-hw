let i = 0;
const image = document.getElementById("img");
const imgs = ['img/2.jpg', 'img/3.JPG', 'img/4.png', 'img/1.jpg'];
function showImg() {
    if (i < imgs.length) {
        image.src = imgs[i];
        i++;
    } else {
        i = 0;
    }
}


const interval = setInterval(showImg, 10000);
const stopBtn = document.createElement('button');
stopBtn.innerText = 'Прекратить';
stopBtn.id = 'stop';
document.querySelector('script').before(stopBtn);

const stop = document.getElementById('stop');
stop.addEventListener('click', () => {
    clearInterval(interval);
})

const startBtn = document.createElement('button');
startBtn.innerText = 'Возобновить показ';
startBtn.id = 'start';
document.getElementById('stop').after(startBtn);

const start = document.getElementById('start');
start.addEventListener('click', () => {
    const interval = setInterval(showImg, 10000);
})