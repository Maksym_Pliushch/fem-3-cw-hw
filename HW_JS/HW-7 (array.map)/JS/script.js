function list(array) {
    const arrayNew = array.map(element => `<li>${element}</li>`);
    const ulList = document.createElement('ul');
    ulList.innerHTML = arrayNew.join('');
    document.querySelector("script").before(ulList);
}

list([1,2,3,4,5,'hello','world']);