function filterBy(array, type) {
    let newArray = [];
    for (let i = 0; i < array.length; i++) {
        if (typeof array[i] !== type) {
            newArray.push(array[i])
        }
    }
    return newArray;
}

console.log(filterBy([12, 13, 16, 'some', null, undefined, [], {}, 'hello', 0, 12], 'string'));