const input = document.createElement('input');
input.setAttribute('placeholder', 'Price');
document.querySelector('script').before(input);
input.addEventListener('focus', () => {
    input.classList.add('brdr-green');
});

input.addEventListener('blur', () => {
    if (input.value < 0) {
        input.classList.add('brdr-red');

        const text = document.createElement('p');
        text.innerText = 'Please enter correct price';
        document.querySelector('input').after(text);
    } else {
        input.classList.remove('brdr-green');

        const span = document.createElement('span');
        document.querySelector('input').after(span);
        span.innerText = `Текущая цена ${input.value}`;

        const button = document.createElement('button');
        button.innerText = ' X ';
        document.querySelector('span').after(button);

        input.style.color = 'green';

        button.onclick = function () {
            button.remove();
            span.remove();
            input.value = '';
        }
    }
});
