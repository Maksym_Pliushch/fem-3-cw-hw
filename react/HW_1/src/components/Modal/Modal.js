import React, {Component} from 'react';
import PropTypes from 'prop-types';
import './Modal.scss'

class Modal extends Component {
  render() {
    const { className, children, header, action } = this.props;
    return (
      <div className={className}>
        <h2 className='modal__header'>{header}</h2>
        {children}
        {action[0]}
        {action[1]}
      </div>
    )
  }
}

Modal.propTypes = {
  className: PropTypes.string,
  action: PropTypes.array,
  header: PropTypes.string,
  children: PropTypes.array,
};

export default Modal;