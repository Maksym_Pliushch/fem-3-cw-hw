import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTimes} from '@fortawesome/free-solid-svg-icons'

import Button from '../Button/Button'
import Modal from "../Modal/Modal";

class ModalContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showModal: false
    };
    this.closeModal = this.closeModal.bind(this);
    this.openModal = this.openModal.bind(this);
  }

  openModal = () => {
    this.setState({showModal: true});
  };

  closeModal = () => {
    this.setState({showModal: false});
  };

  render() {

    const { buttonLabel, backgroundColor, children, header, closeButton } = this.props;
    const { showModal } = this.state;
    const action = [<Button text='Ok' onClick={this.closeModal}/>, <Button text='Cancel' onClick={this.closeModal}/>];

    return(
      <div>
        <Button onClick={this.openModal} text={buttonLabel} backgroundColor={backgroundColor}/>
        {showModal && (
            <div className="modal__drop">
              <Modal action={action} className="modal__content" header = {header}  >
                { closeButton ?
                  <FontAwesomeIcon icon={faTimes} style={{position: 'absolute', right: '15px', top: '15px', color: 'white', cursor: 'pointer'}} onClick={this.closeModal}/>
                  : null}
                {children}
              </Modal>
          </div>)}
      </div>
    )
  }
}

ModalContainer.propTypes = {
  buttonLabel: PropTypes.string,
  backgroundColor: PropTypes.string,
  children: PropTypes.object,
  header: PropTypes.string,
  closeButton: PropTypes.bool,
};

export default ModalContainer;