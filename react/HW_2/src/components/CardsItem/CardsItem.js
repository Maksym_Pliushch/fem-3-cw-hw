import React,{Component} from 'react';
import { faStar } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import ModalContainer from '../ModalContainer/ModalContainer'

class CardsItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isFavorite: false
    };
  }

  addFavorite = () => {
    this.setState({isFavorite: !this.state.isFavorite});
  };

  render() {
    const { title, color, url, article, price, update } = this.props;
    const { isFavorite } = this.state;
    const style = { color: isFavorite ? 'orange' : 'black'};
    return (
      <div>
        <img className='cards__img'
             src={url}
             alt="telephone"/>
        <span className='cards__title'>{title} </span>
        <span className='cards__color'>{color}</span>
        <p onClick={this.addFavorite}
           style={style}>
          <FontAwesomeIcon
            icon={faStar}
            onClick={() => update(this.props)}/></p>
        <p className='cards__article'>This is article item: {article}</p>
        <div style={{display: 'flex', justifyContent: 'space-around'}}>
          <p className='cards__price'>Price: ${price}</p>
          <ModalContainer
            buttonLabel="Add card"
            backgroundColor='orange'
            header='Are you sure to add this item?'
            isIcon/>
        </div>
      </div>
    )
  }
}

export default CardsItem;
