import React, {Component} from 'react';
import PropTypes from 'prop-types';

import './Button.scss'

class Button extends Component{

  render() {
    const { backgroundColor, color, text, onClick } = this.props;
    return (
      <button
        onClick={onClick}
        className="button"
        style={{backgroundColor, color}}>{text}</button>
    )
  }
}

Button.propTypes = {
  backgroundColor: PropTypes.string,
  color: PropTypes.string,
  text: PropTypes.string,
  onClick: PropTypes.func,
};

Button.defaultProps = {
  backgroundColor: '#b3382c',
  color: 'white',
  className: 'button',
  onClick: () => {}
};

export default Button;