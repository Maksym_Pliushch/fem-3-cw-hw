import React, {Component} from 'react';
import PropTypes from 'prop-types';

import Button from '../Button/Button'
import Modal from "../Modal/Modal";

class ModalContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showModal: false
    };
  }

  openModal = () => {
    this.setState({showModal: true});
  };

  onSubmit = () => {
    this.setState({showModal: false})
    console.log('ADD')
  }

  onCancel = () => {
    this.setState({showModal: false})
    console.log('CANCELED')
  }

  render() {

    const { buttonLabel, backgroundColor, children, header, isIcon, isOpen } = this.props;
    const { showModal } = this.state;

    return(
      <div>
        <Button onClick={this.openModal} text={buttonLabel} backgroundColor={backgroundColor}/>
        {showModal && (
            <div className="modal__drop">
              <Modal
                action=''
                className="modal__content"
                header={header}
                onSubmit={this.onSubmit}
                onCancel={this.onCancel}
                isIcon={isIcon}
                isOpen={isOpen}
                >
                {children}
              </Modal>
          </div>)}
      </div>
    )
  }
}

ModalContainer.propTypes = {
  buttonLabel: PropTypes.string,
  backgroundColor: PropTypes.string,
  children: PropTypes.object,
  header: PropTypes.string,
  closeButton: PropTypes.bool,
};

export default ModalContainer;