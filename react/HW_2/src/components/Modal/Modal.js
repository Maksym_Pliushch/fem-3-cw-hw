import React, {Component} from 'react';
import PropTypes from 'prop-types';
import './Modal.scss'
import Button from '../Button/Button'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTimes } from '@fortawesome/free-solid-svg-icons'

class Modal extends Component {
  render() {
    const { className, children, header, onSubmit, onCancel, isIcon } = this.props;
    return (
        <div className={className}>
          <h2 className='modal__header'>{header}</h2>
          { isIcon ? <FontAwesomeIcon
            icon={faTimes}
            style={{position: 'absolute', right: '15px', top: '15px', color: 'white', cursor: 'pointer'}}
            onClick={onCancel}/> : null}
          {children}
          <Button onClick={onSubmit} text='Ok'/>
          <Button onClick={onCancel} text='Cancel'/>
        </div>
    )
  }
}

Modal.propTypes = {
  className: PropTypes.string,
  action: PropTypes.array,
  header: PropTypes.string,
  children: PropTypes.array,
  onSubmit: PropTypes.func,
  onCancel: PropTypes.func,
};

Modal.defaultProps = {
  title: '',
  children: ''
}

export default Modal;