import React,{Component} from 'react';
import CardsItem from "../CardsItem/CardsItem";
import './Cards.scss'

class Cards extends Component {
  constructor(props) {
    super(props);
    this.state = {
      items : [],
      showModal: false,
      isFavorite: false,
      favorite: JSON.parse(localStorage.getItem('item'))  || [],
    };
  }

  componentDidMount() {
    fetch('./data.json')
      .then(response => response.json())
      .then(data => this.setState({
        items: data
      }))
  }

  // addFavorite = () => {
  //   this.setState(prevState => {
  //     return {
  //       isFavorite: !prevState.isFavorite
  //     }
  //   })
  // }

  updateState = (who) => {
    this.setState({favorite: this.state.favorite.concat(who)}, () => {
      localStorage.setItem('item', JSON.stringify(this.state.favorite))
    })
  }
  
  render() {
    const { items, favorite } = this.state;

    return (
        <ul className='cards'>
          {items.map((item) => (
            <li key={item.article} className='cards__item'>
              <CardsItem
                title={item.title}
                price={item.price}
                url={item.url}
                article={item.article}
                color={item.color}
                update={this.updateState}/>
            </li>
          ))}
        </ul>
    )
  }
}

export default Cards;