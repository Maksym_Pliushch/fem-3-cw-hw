import React from 'react';

import ModalContainer from "./components/ModalContainer/ModalContainer";
import Cards from "./components/Cards/Cards";
import data from "./public/data";


class App extends React.Component{

render() {
  return (
    <div>
      <ModalContainer
        buttonLabel="Open first modal"
        backgroundColor="lightblue"
        header="Do you want to delete this file?"
        isIcon
        >
        <div>
          <p> Once you delete this file, it won’t be possible to undo this action.</p>
          <p>Are you sure you want to delete it?</p>
        </div>
      </ModalContainer>

      <ModalContainer
        buttonLabel="Open second modal"
        backgroundColor="purple"
        header="Second Modal"
        isIcon={false}>
        <div>
          <p>This is another Modal</p>
          <p>This is new text</p>
        </div>
      </ModalContainer>

      <Cards items={data}/>
    </div>
    );
  }
}


export default App;
